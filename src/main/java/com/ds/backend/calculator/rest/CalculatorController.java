package com.ds.backend.calculator.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/calculator")
public class CalculatorController {

  @GetMapping
  public ResponseEntity test() {
    return ResponseEntity.ok("Calculator works");
  }
}
